import { Root } from "native-base";
import React from "react";
import { createAppContainer, createStackNavigator, createSwitchNavigator } from "react-navigation";
// Menu Dashboard
import Dashboard from "./screens/dashboard/";
import CheckInScanQRCode from "./screens/dashboard/check-in-scan-qrcode";
import CheckInScanQRCodeMaintenance from "./screens/dashboard/check-in-scan-qrcode-maintenance";
import CheckOutScanQRCodeMaintenance from "./screens/dashboard/check-out-scan-qrcode-maintenance";
import CheckOutScanQRCodePerbaikan from "./screens/dashboard/check-out-scan-qrcode-perbaikan";
import DetailAsetHistory from "./screens/dashboard/detail-aset-history";
import DetailAsetHistoryPartEight from "./screens/dashboard/detail-aset-history-eight";
import DetailAsetHistoryPartFive from "./screens/dashboard/detail-aset-history-five";
import DetailAsetHistoryPartFour from "./screens/dashboard/detail-aset-history-four";
import DetailAsetHistoryMaintenance from "./screens/dashboard/detail-aset-history-maintenance";
import DetailAsetHistoryPartSeven from "./screens/dashboard/detail-aset-history-seven";
import DetailAsetHistoryPartSix from "./screens/dashboard/detail-aset-history-six";
import DetailAsetHistoryPartThree from "./screens/dashboard/detail-aset-history-three";
import DetailAsetHistoryPartTwo from "./screens/dashboard/detail-aset-history-two";
import DetailTiketMaintenance from "./screens/dashboard/detail-tiket-maintenance";
import DetailTiketPerbaikan from "./screens/dashboard/detail-tiket-perbaikan";
import HasilPekerjaan from "./screens/dashboard/hasil-pekerjaan";
import HasilPekerjaanMaintenance from "./screens/dashboard/hasil-pekerjaan-maintenance";
import ProsesIdentifikasiPartOne from "./screens/dashboard/proses-identifikasi-part-one";
import ProsesIdentifikasiPartOneMaintenance from "./screens/dashboard/proses-identifikasi-part-one-maintenance";
import ProsesIdentifikasiPartThree from "./screens/dashboard/proses-identifikasi-part-three";
import ProsesIdentifikasiPartThreeMaintenance from "./screens/dashboard/proses-identifikasi-part-three-maintenance";
import ProsesIdentifikasiPartTwo from "./screens/dashboard/proses-identifikasi-part-two";
import ProsesIdentifikasiPartTwoMaintenance from "./screens/dashboard/proses-identifikasi-part-two-maintenance";
import ReviewHasilPekerjaan from "./screens/dashboard/review-hasil-pekerjaan";
import SparePartListViewPartTwo from "./screens/dashboard/spare-part-list-view-part-two";
import DetailSparePartTwo from "./screens/dashboard/sparepart-detail-two";
import TiketKeluhanMaintenance from "./screens/dashboard/tiket-keluhan-maintenance";
import TiketKeluhanPerbaikan from "./screens/dashboard/tiket-keluhan-perbaikan";
import TiketKeluhanPerbaikanTwo from "./screens/dashboard/tiket-keluhan-perbaikan-two";
// Tiket Maintenance
import TiketMaintenance from "./screens/dashboard/tiket-maintenance";
// Tiket Perbaikan
import TiketPerbaikan from "./screens/dashboard/tiket-perbaikan";
import ViewPermintaanMaintenance from "./screens/dashboard/view-permintaan-maintenance";
import ViewPermintaanMaintenancePartFive from "./screens/dashboard/view-permintaan-maintenance-five";
import ViewPermintaanMaintenancePartFour from "./screens/dashboard/view-permintaan-maintenance-four";
import ViewPermintaanMaintenancePartThree from "./screens/dashboard/view-permintaan-maintenance-three";
import ViewPermintaanMaintenancePartTwo from "./screens/dashboard/view-permintaan-maintenance-two";
import ViewPermintaanPerbaikan from "./screens/dashboard/view-permintaan-perbaikan";
import ViewPermintaanPerbaikanPartEight from "./screens/dashboard/view-permintaan-perbaikan-part-eight";
import ViewPermintaanPerbaikanPartFive from "./screens/dashboard/view-permintaan-perbaikan-part-five";
import ViewPermintaanPerbaikanPartFour from "./screens/dashboard/view-permintaan-perbaikan-part-four";
import ViewPermintaanPerbaikanPartSeven from "./screens/dashboard/view-permintaan-perbaikan-part-seven";
import ViewPermintaanPerbaikanPartSix from "./screens/dashboard/view-permintaan-perbaikan-part-six";
import ViewPermintaanPerbaikanPartTwo from "./screens/dashboard/view-permintaan-perbaikan-part-two";
import ViewPermintaanPerbaikanPartThree from "./screens/dashboard/view-permintaan-perbaikan-three";
// Menu Auth
import LoginUser from "./screens/loginUser/";
import ForgotPassword from "./screens/loginUser/forgot-password";
// Menu Profile
import MyProfile from "./screens/myProfile/";
import DetailProfil from "./screens/myProfile/detail-profil";
import EditProfil from "./screens/myProfile/edit-profil";
import UbahKataSandi from "./screens/myProfile/ubdah-kata-sandi";
// Riwayat
import MenuNotifPerbaikan from "./screens/notif/";
import DetailAsetHistoryPartSixteen from "./screens/notif/detail-aset-history-sixteen";
import HasilPekerjaanPartThree from "./screens/notif/hasil-pekerjaan-three";
import MenuRiwayatMaintenance from "./screens/notif/menu-riwayat-maintenance";
import ViewPermintaanPerbaikanPartFourteen from "./screens/notif/view-permintaan-perbaikan-part-fourteen";
import TiketKeluhanPerbaikanFour from "./screens/notif/tiket-keluhan-perbaikan-four";
import ViewPermintaanPerbaikanPartFifteen from "./screens/notif/view-permintaan-perbaikan-part-fIfteen";
import DetailAsetHistoryPartSeventeen from "./screens/notif/detail-aset-history-seventeen";
// Sparepart
import SparePartListView from "./screens/sparepart/";
import DetailSparePart from "./screens/sparepart/sparepart-detail";
// Tiket
// Tiket Perbaikan
import MenuTiketPerbaikan from "./screens/tiket/";
import CheckInScanQRCodeMaintenancePartTwo from "./screens/tiket/check-in-scan-qrcode-maintenance-two";
import CheckInScanQRCodeTwo from "./screens/tiket/check-in-scan-qrcode-two";
import CheckOutScanQRCodePerbaikanPartTwo from "./screens/tiket/check-out-scan-qrcode-perbaikan-two";
import DetailAsetHistoryPartEleven from "./screens/tiket/detail-aset-history-eleven";
import DetailAsetHistoryPartFifteen from "./screens/tiket/detail-aset-history-fifteen";
import DetailAsetHistoryPartFourteen from "./screens/tiket/detail-aset-history-fourteen";
import DetailAsetHistoryPartNine from "./screens/tiket/detail-aset-history-nine";
import DetailAsetHistoryPartTen from "./screens/tiket/detail-aset-history-ten";
import DetailAsetHistoryPartThirteen from "./screens/tiket/detail-aset-history-thirteen";
import DetailAsetHistoryPartTwelve from "./screens/tiket/detail-aset-history-twelve";
import DetailTiketMaintenancePartTwo from "./screens/tiket/detail-tiket-maintenance-two";
import DetailTiketPerbaikanPartTwo from "./screens/tiket/detail-tiket-perbaikan-two";
import HasilPekerjaanPartTwo from "./screens/tiket/hasil-pekerjaan-two";
// Tiket
// Tiket Maintenance
import MenuTiketMaintenance from "./screens/tiket/menu-tiket-maintenance";
import PilihSparePartOne from "./screens/tiket/pilih-sparepart-part-one";
import PilihSparePartTwo from "./screens/tiket/pilih-sparepart-part-two";
import ProsesIdentifikasiPartFive from "./screens/tiket/proses-identifikasi-part-five";
import ProsesIdentifikasiPartFiveMaintenance from "./screens/tiket/proses-identifikasi-part-five-maintenance";
import ProsesIdentifikasiPartFour from "./screens/tiket/proses-identifikasi-part-four";
import ProsesIdentifikasiPartFourMaintenance from "./screens/tiket/proses-identifikasi-part-four-maintenance";
import DetailSparePartThree from "./screens/tiket/sparepart-detail-three";
import TiketKeluhanPerbaikanThree from "./screens/tiket/tiket-keluhan-perbaikan-three";
import ViewPermintaanMaintenancePartEight from "./screens/tiket/view-permintaan-maintenance-eight";
import ViewPermintaanMaintenancePartSeven from "./screens/tiket/view-permintaan-maintenance-seven";
import ViewPermintaanMaintenancePartSix from "./screens/tiket/view-permintaan-maintenance-six";
import ViewPermintaanPerbaikanPartEleven from "./screens/tiket/view-permintaan-perbaikan-part-eleven";
import ViewPermintaanPerbaikanPartNine from "./screens/tiket/view-permintaan-perbaikan-part-nine";
import ViewPermintaanPerbaikanPartTen from "./screens/tiket/view-permintaan-perbaikan-part-ten";
import ViewPermintaanPerbaikanPartThirteen from "./screens/tiket/view-permintaan-perbaikan-part-thirteen";
import ViewPermintaanPerbaikanPartTwelve from "./screens/tiket/view-permintaan-perbaikan-part-twelve";
















// Menu Authentication
const AuthStack = createStackNavigator(
	{
		LoginUser: { screen: LoginUser },
		ForgotPassword: { screen: ForgotPassword }
	},
	{
		headerMode: "none",
		initialRouteParams: "LoginUser"
	}
);

// Menu Dashboard
const DashboardStack = createStackNavigator(
	{
    // Dashboard Utama
    Dashboard: { screen: Dashboard },
    
    // Tiket Perbaikan
    ReviewHasilPekerjaan: { screen: ReviewHasilPekerjaan },
    TiketPerbaikan: { screen: TiketPerbaikan },
    DetailTiketPerbaikan: { screen: DetailTiketPerbaikan },
    ViewPermintaanPerbaikan: { screen: ViewPermintaanPerbaikan },
    ViewPermintaanPerbaikanPartTwo: { screen: ViewPermintaanPerbaikanPartTwo },
    ViewPermintaanPerbaikanPartThree: { screen: ViewPermintaanPerbaikanPartThree },
    ViewPermintaanPerbaikanPartFour: { screen: ViewPermintaanPerbaikanPartFour },
    ViewPermintaanPerbaikanPartFive: { screen: ViewPermintaanPerbaikanPartFive },
    ViewPermintaanPerbaikanPartSix: { screen: ViewPermintaanPerbaikanPartSix },
    ViewPermintaanPerbaikanPartSeven: { screen: ViewPermintaanPerbaikanPartSeven },
    ViewPermintaanPerbaikanPartEight: { screen: ViewPermintaanPerbaikanPartEight },
    
    DetailAsetHistory: { screen: DetailAsetHistory },
    DetailAsetHistoryPartTwo: { screen: DetailAsetHistoryPartTwo },
    DetailAsetHistoryPartThree: { screen: DetailAsetHistoryPartThree },
    DetailAsetHistoryPartFour: { screen: DetailAsetHistoryPartFour },
    DetailAsetHistoryPartFive: { screen: DetailAsetHistoryPartFive },
    DetailAsetHistoryPartSix: { screen: DetailAsetHistoryPartSix },
    DetailAsetHistoryPartSeven: { screen: DetailAsetHistoryPartSeven },
    DetailAsetHistoryPartEight: { screen: DetailAsetHistoryPartEight },
    
    ProsesIdentifikasiPartOne: { screen: ProsesIdentifikasiPartOne },
    ProsesIdentifikasiPartTwo: { screen: ProsesIdentifikasiPartTwo },
    ProsesIdentifikasiPartThree: { screen: ProsesIdentifikasiPartThree },

    SparePartListViewPartTwo: { screen: SparePartListViewPartTwo },
    DetailSparePartTwo: { screen: DetailSparePartTwo },

    HasilPekerjaan: { screen: HasilPekerjaan },
    CheckInScanQRCode: { screen: CheckInScanQRCode },

    CheckOutScanQRCodePerbaikan: { screen: CheckOutScanQRCodePerbaikan },
    TiketKeluhanPerbaikan: { screen: TiketKeluhanPerbaikan },
    TiketKeluhanPerbaikanTwo: { screen: TiketKeluhanPerbaikanTwo },

    // Tiket Maintenance
    TiketMaintenance: { screen: TiketMaintenance },
    DetailTiketMaintenance: { screen: DetailTiketMaintenance },
    ViewPermintaanMaintenance: { screen: ViewPermintaanMaintenance },
    ViewPermintaanMaintenancePartTwo: { screen: ViewPermintaanMaintenancePartTwo },
    ViewPermintaanMaintenancePartThree: { screen: ViewPermintaanMaintenancePartThree },
    ViewPermintaanMaintenancePartFour: { screen: ViewPermintaanMaintenancePartFour },
    ViewPermintaanMaintenancePartFive: { screen: ViewPermintaanMaintenancePartFive },
    DetailAsetHistoryMaintenance: { screen: DetailAsetHistoryMaintenance },
    ProsesIdentifikasiPartOneMaintenance: { screen: ProsesIdentifikasiPartOneMaintenance },
    ProsesIdentifikasiPartTwoMaintenance: { screen: ProsesIdentifikasiPartTwoMaintenance },
    ProsesIdentifikasiPartThreeMaintenance: { screen: ProsesIdentifikasiPartThreeMaintenance },
    HasilPekerjaanMaintenance: { screen: HasilPekerjaanMaintenance },
    CheckInScanQRCodeMaintenance: { screen: CheckInScanQRCodeMaintenance },
    CheckOutScanQRCodeMaintenance: { screen: CheckOutScanQRCodeMaintenance },
    TiketKeluhanMaintenance: { screen: TiketKeluhanMaintenance },
	},
	{
		headerMode: "none",
		initialRouteParams: "Dashboard"
	}
);

// Menu Spare Part
const SparePartStack = createStackNavigator(
	{
		// Spaerpart
    SparePartListView: { screen: SparePartListView },
    DetailSparePart: { screen: DetailSparePart },
	},
	{
		headerMode: "none",
		initialRouteParams: "SparePartListView"
	}
);

// Menu Tiket
const TiketStack = createStackNavigator(
	{
    // Tiket Perbaikan
    MenuTiketPerbaikan: { screen: MenuTiketPerbaikan },
    DetailTiketPerbaikanPartTwo: { screen: DetailTiketPerbaikanPartTwo },
    ViewPermintaanPerbaikanPartNine: { screen: ViewPermintaanPerbaikanPartNine },
    DetailAsetHistoryPartNine: { screen: DetailAsetHistoryPartNine },
    DetailAsetHistoryPartTen: { screen: DetailAsetHistoryPartTen },
    DetailAsetHistoryPartEleven: { screen: DetailAsetHistoryPartEleven },
    DetailAsetHistoryPartTwelve: { screen: DetailAsetHistoryPartTwelve },
    DetailAsetHistoryPartFourteen: { screen: DetailAsetHistoryPartFourteen },
    
    CheckInScanQRCodeTwo: { screen: CheckInScanQRCodeTwo },

    ProsesIdentifikasiPartFour: { screen: ProsesIdentifikasiPartFour },
    ProsesIdentifikasiPartFive: { screen: ProsesIdentifikasiPartFive },

    ViewPermintaanPerbaikanPartTen: { screen: ViewPermintaanPerbaikanPartTen },
    HasilPekerjaanPartTwo: { screen: HasilPekerjaanPartTwo },
    TiketKeluhanPerbaikanThree: { screen: TiketKeluhanPerbaikanThree },
    ViewPermintaanPerbaikanPartTwelve: { screen: ViewPermintaanPerbaikanPartTwelve },
    ViewPermintaanPerbaikanPartEleven: { screen: ViewPermintaanPerbaikanPartEleven },
    ViewPermintaanPerbaikanPartThirteen: { screen: ViewPermintaanPerbaikanPartThirteen },
    CheckOutScanQRCodePerbaikanPartTwo: { screen: CheckOutScanQRCodePerbaikanPartTwo },

    PilihSparePartOne: { screen: PilihSparePartOne },

    // Tiket Maintenance
    MenuTiketMaintenance: { screen: MenuTiketMaintenance },
    DetailTiketMaintenancePartTwo: { screen: DetailTiketMaintenancePartTwo },
    ViewPermintaanMaintenancePartSix: { screen: ViewPermintaanMaintenancePartSix },
    ViewPermintaanMaintenancePartSeven: { screen: ViewPermintaanMaintenancePartSeven },
    ViewPermintaanMaintenancePartEight: { screen: ViewPermintaanMaintenancePartEight },
    DetailAsetHistoryPartThirteen: { screen: DetailAsetHistoryPartThirteen },
    DetailAsetHistoryPartFifteen: { screen: DetailAsetHistoryPartFifteen },
    CheckInScanQRCodeMaintenancePartTwo: { screen: CheckInScanQRCodeMaintenancePartTwo },
    ProsesIdentifikasiPartFourMaintenance: { screen: ProsesIdentifikasiPartFourMaintenance },
    ProsesIdentifikasiPartFiveMaintenance: { screen: ProsesIdentifikasiPartFiveMaintenance },

    PilihSparePartTwo: { screen: PilihSparePartTwo },
    DetailSparePartThree: { screen: DetailSparePartThree },
	},
	{
		headerMode: "none",
		initialRouteParams: "MenuTiketPerbaikan"
	}
);

// Menu Riwayat
const RiwayatStack = createStackNavigator(
	{
    // Riwayat
    MenuNotifPerbaikan: { screen: MenuNotifPerbaikan },
    MenuRiwayatMaintenance: { screen: MenuRiwayatMaintenance },
    ViewPermintaanPerbaikanPartFourteen: { screen: ViewPermintaanPerbaikanPartFourteen },
    DetailAsetHistoryPartSixteen: { screen: DetailAsetHistoryPartSixteen },
    HasilPekerjaanPartThree: { screen: HasilPekerjaanPartThree },
    TiketKeluhanPerbaikanFour: { screen: TiketKeluhanPerbaikanFour },
    ViewPermintaanPerbaikanPartFifteen: { screen: ViewPermintaanPerbaikanPartFifteen },
    DetailAsetHistoryPartSeventeen: { screen: DetailAsetHistoryPartSeventeen },
	},
	{
		headerMode: "none",
		initialRouteParams: "MenuNotifPerbaikan"
	}
);

// Menu Profile
const ProfileStack = createStackNavigator(
	{
    // Profile
    MyProfile: { screen: MyProfile },
    EditProfil: { screen: EditProfil },
    DetailProfil: { screen: DetailProfil },
    UbahKataSandi: { screen: UbahKataSandi },
	},
	{
		headerMode: "none",
		initialRouteParams: "MyProfile"
	}
);

const AppNavigator = createSwitchNavigator(
  {
    AuthStack,
    DashboardStack,
    SparePartStack,
    TiketStack,
    RiwayatStack,
    ProfileStack
  },
  {
    initialRouteName: "AuthStack",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default () =>
  <Root>
    <AppContainer />
  </Root>;
